/*  ISA RANDRA  */

import java.text.DecimalFormat;
import java.util.Scanner;

public class CalculatorLuasVolume {
    public static void main(String[] args){
        // inisialisasi variablenya sengaja saya taruh di sini agar berada di luar loop utama program
        int control, controlLuas, controlVolume;
        double sisiPersegi, luasPersegi, jariJariLingkaran, luasLingkaran, alasSegitiga, tinggiSegitiga, luasSegitiga, panjangPersegi, lebarPersegi, luasPersegiPanjang, sisiKubus, volumeKubus, panjangBalok, lebarBalok, tinggiBalok, volumeBalok, tinggiTabung, jariJariTabung, volumeTabung;

        header("Kalkulator Penghitung Luas dan Volume");

        Scanner scanner = new Scanner(System.in);
        DecimalFormat decimalFormat = new DecimalFormat("##.00");       // buat round to second decimals place

        // loop untuk program. program akan terus berjalan hingga user menginput 0
        do {
            System.out.println("Menu");
            System.out.println("1. Hitung Luas Bidang\n2. Hitung Volume Bidang\n0. Tutup Aplikasi");
            garis();

            System.out.print("Pilih menu: ");
            control = scanner.nextInt();

            // control flow untuk memilih apa yang ingin dihitung (luas or volume). 0 untuk menutup aplikasi
            if (control == 1){
                header("Pilih bidang yang akan dihitung luasnya");
                System.out.println("1. Persegi\n2. Lingkaran\n3. Segitiga\n4. Persegi panjang\n0. Kembali ke menu utama");
                garis();

                System.out.print("Pilih menu: ");
                controlLuas = scanner.nextInt();

                // control flow untuk memilih bidang apa yang ingin dihitung luasnya. 0 untuk kembali ke menu utama
                switch (controlLuas){
                    case 1:
                        header("Anda memilih persegi");

                        System.out.print("Masukkan sisi: ");
                        sisiPersegi = scanner.nextDouble();

                        processing();

                        luasPersegi = sisiPersegi * sisiPersegi;

                        System.out.println("Luas dari persegi adalah " + decimalFormat.format(luasPersegi));
                        garis();
                        pressEnterToContinue();
                        break;
                    case 2:
                        header("Anda memilih lingkaran");

                        System.out.print("Masukkan jari-jari lingkaran: ");
                        jariJariLingkaran = scanner.nextDouble();

                        processing();

                        luasLingkaran = Math.PI * jariJariLingkaran * jariJariLingkaran;

                        System.out.println("Luas dari lingkaran adalah " + decimalFormat.format(luasLingkaran));
                        garis();
                        pressEnterToContinue();
                        break;
                    case 3:
                        header("Anda memilih segitiga");

                        System.out.print("Masukkan alas segitiga: ");
                        alasSegitiga = scanner.nextDouble();
                        System.out.print("Masukkan tinggi segitiga: ");
                        tinggiSegitiga = scanner.nextDouble();

                        processing();

                        luasSegitiga = 0.5 * alasSegitiga * tinggiSegitiga;

                        System.out.println("Luas dari segitiga adalah " + decimalFormat.format(luasSegitiga));
                        garis();
                        pressEnterToContinue();
                        break;
                    case 4:
                        header("Anda memilih persegi panjang");

                        System.out.print("Masukkan panjang: ");
                        panjangPersegi = scanner.nextDouble();
                        System.out.print("Masukkan lebar: ");
                        lebarPersegi = scanner.nextDouble();

                        processing();

                        luasPersegiPanjang = panjangPersegi * lebarPersegi;

                        System.out.println("Luas dari persegi panjang adalah " + decimalFormat.format(luasPersegiPanjang));
                        garis();
                        pressEnterToContinue();
                        break;
                    case 0:
                        header("Kembali ke menu utama");
                        break;
                }
            }
            else if (control == 2){
                header("Pilih bidang yang akan dihitung volumenya");
                System.out.println("1. Kubus\n2. Balok\n3. Tabung\n0. Kembali ke menu utama");
                garis();

                System.out.print("Pilih menu: ");
                controlVolume = scanner.nextInt();

                // control flow untuk memilih bidang apa yang ingin dihitung volumenya. 0 untuk kembali ke menu utama
                switch (controlVolume){
                    case 1:
                        header("Anda memilih kubus");

                        System.out.print("Masukkan sisi: ");
                        sisiKubus = scanner.nextDouble();

                        processing();

                        volumeKubus = sisiKubus * sisiKubus * sisiKubus;

                        System.out.println("Volume dari kubus adalah " + decimalFormat.format(volumeKubus));
                        garis();
                        pressEnterToContinue();
                        break;
                    case 2:
                        header("Anda memilih balok");

                        System.out.print("Masukkan panjang: ");
                        panjangBalok = scanner.nextDouble();
                        System.out.print("Masukkan lebar: ");
                        lebarBalok = scanner.nextDouble();
                        System.out.print("Masukkan tinggi: ");
                        tinggiBalok = scanner.nextDouble();

                        processing();

                        volumeBalok = panjangBalok * lebarBalok * tinggiBalok;

                        System.out.println("Volume dari balok adalah " + decimalFormat.format(volumeBalok));
                        garis();
                        pressEnterToContinue();
                        break;
                    case 3:
                        header("Anda memilih tabung");

                        System.out.print("Masukkan tinggi: ");
                        tinggiTabung = scanner.nextDouble();
                        System.out.print("Masukkan jari-jari: ");
                        jariJariTabung = scanner.nextDouble();

                        processing();

                        volumeTabung = Math.PI * tinggiTabung * jariJariTabung * jariJariTabung;

                        System.out.println("Volume dari tabung adalah " + decimalFormat.format(volumeTabung));
                        garis();
                        pressEnterToContinue();
                        break;
                    case 0:
                        header("Kembali ke menu utama");
                        break;
                }
            }
            else if (control == 0){
                garis();
                System.out.println("Aplikasi ditutup");
                scanner.close();
                break;
            }
        } while (true);
    }

    /**
     * method untuk menampilkan "tekan enter untuk kembali ke menu utama"
     */
    private static void pressEnterToContinue(){
        System.out.println("Tekan enter untuk kembali ke menu utama");
        garis();
        try {
            System.in.read();
        } catch(Exception e){
        }
    }

    /**
     * method untuk menampilkan teks yang disandwich dengan garis
     * @param string teks yang ingin ditampilkan di antara dua garis
     */
    private static void header(String string){
        garis();
        System.out.println(string);
        garis();
    }

    /**
     * menampilkan garis horizontal
     */
    private static void garis(){
        System.out.println("------------------------------------------");
    }

    /**
     * menampilkan kalimat "processing..."
     */
    private static void processing(){
        System.out.println(" ");
        System.out.println("processing...");
        System.out.println(" ");
    }
}
